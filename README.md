<!--
SPDX-FileCopyrightText: 2022 Marcel Schilling <foss@mschilli.com>

SPDX-License-Identifier: FSFAP
-->

[#]: # (README Markdown file for `gendercol` UserScript)
[#]: #
[#]: # (Copyright [C] 2022  Marcel Schilling <foss@mschilli.com>)
[#]: #
[#]: # (This file is part of `gendercol`.)
[#]: #
[#]: # (`gendercol` is free software: you can redistribute it and/or)
[#]: # (modify it under the terms of the GNU Affero General Public)
[#]: # (License as published by the Free Software Foundation, either)
[#]: # (version 3 of the License, or [at your option] any later)
[#]: # (version.)
[#]: #
[#]: # (This program is distributed in the hope that it will be useful,)
[#]: # (but WITHOUT ANY WARRANTY; without even the implied warranty of)
[#]: # (MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the)
[#]: # (GNU Affero General Public License for more details.)
[#]: #
[#]: # (You should have received a copy of the GNU Affero General Public)
[#]: # (License along with this program.  If not, see)
[#]: # (<http://www.gnu.org/licenses/>.)
[#]: #
[#]: # (Copying and distribution of this file, with or without)
[#]: # (modification, are permitted in any medium without royalty)
[#]: # (provided the copyright notice and this notice are preserved.)
[#]: # (This file is offered as-is, without any warranty.)


[#]: # (#####################)
[#]: # ( General Information )
[#]: # (#####################)

[#]: # (File:    README.md)
[#]: # (Created: 2022-07-10)
[#]: # (Author:  Marcel Schilling <foss@mschilli.com>)
[#]: # (License: GNU All-Permissive License)
[#]: # (Purpose: Document `gendercol` UserScript.)


[#]: # (###################################)
[#]: # ( Changelog [reverse chronological] )
[#]: # (###################################)

[#]: # (2022-07-10:)
[#]: # ( * Initial version:)
[#]: # (   * Header comments.)
[#]: # (   * Description, License, Installation, and Dedication sections.)


[#]: # (########)
[#]: # ( README )
[#]: # (########)


# `gendercol`

[`UserScript`](https://en.wikipedia.org/wiki/Userscript) to color German
nouns by gender.


## Description

This script augments web pages adding a colored frame around German
nouns to indicate their gender:

* [DarkMagenta](https://www.w3schools.com/colors/color_tryit.asp?color=DarkMagenta): female
* [MidnightBlue](https://www.w3schools.com/colors/color_tryit.asp?color=MidnightBlue): male
* [Peru](https://www.w3schools.com/colors/color_tryit.asp?color=Peru): neutral
* [lightpurple](https://www.w3schools.com/colors/color_tryit.asp?color=lightpurple): female or male
* [pink](https://www.w3schools.com/colors/color_tryit.asp?color=pink): female or neutral
* [darkpurple](https://www.w3schools.com/colors/color_tryit.asp?color=darkpurple): male or neutral
* [grey](https://www.w3schools.com/colors/color_tryit.asp?color=grey): female, male, or neutral

Note that German nouns can have several genders. Usually those change the meaning.

## License

Copyright © 2022 [Marcel Schilling](mailto:foss@mschilli.com)

This program is free software: you can redistribute it and/or modify it
under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Affero General Public License for more details.

You should have received [a copy of the GNU Affero General Public
License](inst/LICENSES/AGPL-3.0-or-later.txt) along with this program.
If not, see <http://www.gnu.org/licenses/>.


While not included in the source repository of `gendercol`, the final
script relies on a dictionary mapping German nouns to their respective
gender(s), which by default is parsed from [the latest XML
dump](https://dumps.wikimedia.org/dewiktionary/latest) of [the
German Wiktionary](https://de.wiktionary.org).
These records are published under the [GNU Free Documentation License]
(https://www.gnu.org/licenses/fdl-1.3.html) (GFDL) and the
[Creative Commons Attribution-Share-Alike 3.0
License](https://creativecommons.org/licenses/by-sa/3.0/) (CC BY-SA
3.0).


## Installation

To include the (separately published and licensed, see
[above](#Licence)) dictionary mapping the German nouns to their
respective gender from [the latest XML
dump](https://dumps.wikimedia.org/dewiktionary/latest) of [the German
Wiktionary](https://de.wiktionary.org) and thus obtain a functional
UserScript, simply run

```sh
./update_dictionary.sh
```

from within the `gendercol` source directory.

Afterwards, install (and enable) it in you web browser (typically by
using a script manager like [GreaseMonkey](https://www.greasespot.net)
[Firefox] or [ViolentMonkey](https://violentmonkey.github.io) [all
popular browser]).


## Dedication

This is my very first UserScript and the first time I am writing
anything in JavaScript.
I am sharing it with the world out of principle but I wrote it for a
specific person that also had the idea for such a script.

I am not sure if this person would like to be mentioned by name in a
public piece of code on the internet, so I'll just say this:
You know who you are.
I hope you like it.
Don't try to learn from this mess of a script.
It might be the ugliest piece of code I've ever written.
Let's blame JavaScript for that.
But I got it to work and I'll happily implement any changes based on
your feedback.
